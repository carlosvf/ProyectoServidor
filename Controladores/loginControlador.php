<?php

namespace blog;

session_start();

require '../vendor/autoload.php';
require_once '../Clases/dbconfig.php';

$error = "";


if (isset($_REQUEST['enviarLogin'])) {

    $usuario = new Usuario();
    $usuario->selectYValida($_REQUEST['usuario'], $_REQUEST['password']);


    if ($usuario->nombre) {
        if ($usuario->estado == "A") {
            $_SESSION['nombreusuario'] = $usuario->usuario;
            $_SESSION['esAdmin'] = $usuario->admin;
            $_SESSION['estadoUsuario'] = $usuario->estado;
            $_SESSION['loginok'] = true;
            header('Location: blogControlador.php');
        } else {
            if ($usuario->estado == "B") {
                $error = "El usuario con el que intenta acceder está bloqueado.";
            } else {
                $error = "El usuario con el que intenta acceder no está activo.";
            }
        }
    } else {
        $error = "Los datos introducidos no son correctos.";
    }
}

require_once '../Vistas/login.php';
?>