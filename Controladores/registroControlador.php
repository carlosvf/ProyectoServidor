<?php

namespace blog;

session_start();

require '../vendor/autoload.php';
require_once '../Clases/dbconfig.php';


if (isset($_REQUEST['enviarRegistro'])) {
    $usuario = new Usuario();
    $usuario->nuevoUsuario($_REQUEST['nombre'], $_REQUEST['correoElectronico'], $_REQUEST['nombreUsuario'], $_REQUEST['contrasenaRegistro']);

    header('Location: ../Controladores/loginControlador.php');
}

require_once '../Vistas/registro.php';
?>