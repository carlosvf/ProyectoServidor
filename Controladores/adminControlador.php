<?php

namespace blog;

use PDO;

require_once '../vendor/autoload.php';

/* PANEL DE CATEGORIAS */
if (isset($_REQUEST['crearCategoria'])) {
    $categoria = new Categoria();
    $categoria->descripcion = $_REQUEST['nuevaCategoria'];
    $categoria->create();
}

$categoria = new Categoria();
$arrayCategorias = $categoria->getAll();

/* ================= PANEL DE USUARIOS NUEVOS ================= */

if (isset($_REQUEST['aprobarUsuario'])) {
    // Traigo el usuario por su id
    $usuario = new Usuario();
    $usuario->select($_REQUEST['idUsuario']);
    // Realizo la función correspondiente
    switch ($_REQUEST['aprobarUsuario']) {
        case 'Aprobar':
            $usuario->estado = 'A'; // Usuario aprobado
            $usuario->save();
            break;
        case 'Bloquear':
            $usuario->estado = 'B'; // Usuario Bloqueado
            $usuario->save();
            break;
        default:
            $usuario->estado = 'P'; // Usuario sigue pendiente (por si acaso)
            $usuario->save();
            break;
    }
}

//TODO hacer autoload para las clases nuevas
$usuario = new Usuario();
$arrayUsuariosSinAprobar = $usuario->getBy('estado', 'P');
/* ==fin panel================================================= */


/* ================= PANEL DE ENTRADAS NUEVAS ================= */


// Array de prueba
//$entrada1 = array('id' => 1, 'texto' => 'Lorem ipsum dolor sit amet, consectetur adipiscing...');
//$entrada2 = array('id' => 2, 'texto' => 'Lorem ipsum dolor sit amet, consectetur adipiscing...');
//$entrada3 = array('id' => 3, 'texto' => 'Lorem ipsum dolor sit amet, consectetur adipiscing...');


if (isset($_REQUEST['aprobarEntrada'])) {
    // Traigo la entrada por su id
    $entrada = new Entrada();
    $entrada->select($_REQUEST['idEntrada']);

    // Realizo la función correspondiente
    switch ($_REQUEST['aprobarEntrada']) {
        case 'Aprobar':
            $entrada->aceptada = 'S'; // Entrada aceptada
            $entrada->save();
            break;
        case 'Borrar':
            $entrada->deletebyId($_REQUEST['idEntrada']); // Borramos entrada
            break;
        default:
            $entrada->aceptada = 'N'; // Entrada sigue sin aceptar (por si acaso)
            $entrada->save();
            break;
    }
}

$entrada = new Entrada();
$arrayEntradasSinAprobar = $entrada->getBy('aceptada', 'N');
/* ==fin panel================================================= */

/* ================= PANEL DE COMENTARIOS NUEVOS ================= */
//$entrada = new Entrada();
//$arrayEntradasSinAprobar = $entrada->getBy('aceptado', 'N');
// Array de prueba
$comentario1 = array('id' => 1, 'texto' => 'comentario1');
$comentario2 = array('id' => 2, 'texto' => 'comentario2');
$comentario3 = array('id' => 3, 'texto' => 'comentario3');
$arrayComentariosSinAprobar = array($comentario1, $comentario2, $comentario3);

if (isset($_REQUEST['aprobarComentario'])) {
    // Traigo el comentario por su id
    $comentarioNeutro = new Comentario();
    $comentario = $comentarioNeutro->getbyId($_REQUEST['idComentario']);
    // Realizo la función correspondiente
    switch ($_REQUEST['aprobarComentario']) {
        case 'Aprobar':
            $comentario->aceptado = 'S'; // Entrada aceptada
            $comentario->save();
            break;
        case 'Borrar':
            $comentario->deletebyId($_REQUEST['idComentario']); // Borramos entrada
            break;
        default:
            $comentario->aceptado = 'N'; // Entrada sigue sin aceptar (por si acaso)
            $comentario->save();
            break;
    }
}
/* ==fin panel================================================= */


require_once '../Vistas/panelAdmin.php';


