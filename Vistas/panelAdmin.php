<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>BLOG</title>
    </head>

    <body>
        <link rel="stylesheet" href="../css/blogPrueba.css">
        <section id="formularios">
            
            <div id='formCategorias' class="form"> 
                <form method='get'>
                    <fieldset>
                        <legend>Panel de Categorías </legend>
                        <div id="divCategorias" class='campo'>
                            <label for='categorias' >Categorías: </label><br/>
                            <select name='categorias' id='categorias' size="10">

                                <?php
                                foreach ($arrayCategorias as $categoria) {
                                    echo "<option>" . $categoria['descripcion'] . "</option>";
                                    ?>
                                    <?php
                                }
                                ?>
                            </select><br><br>
                        </div>
                        <div id="divNuevaCategoria" class='campo'>
                            <label for='nuevaCategoria' >Nueva categoría: </label>
                            <input type='text' name='nuevaCategoria' id='nuevaCategoria'/>
                            <input type='submit' name='crearCategoria' class='boton' value='Crear' />
                        </div>
                    </fieldset>
                </form>
            </div>

            <div id="formUsuarios" class="form">
                <fieldset >
                    <legend>Panel de Usuarios Nuevos </legend>
                    <table>
                        <?php
                        foreach ($arrayUsuariosSinAprobar as $usuario) {
                            ?>
                            <tr>
                            <form id = '<?php $usuario['id'] ?>' method = 'get'>
                                <input type = 'hidden' name = 'idUsuario' value = "<?php echo $usuario['id'] ?>"/>
                                <?php
//                            echo "<a href = 'usuario.php?codigo={$usuario['cod']}'>{$usuario['nombre_corto']}: {$usuario['PVP']} €</a></p>";
                                echo "<td>{$usuario['id']}. {$usuario['usuario']}&nbsp &nbsp</td>";
                                ?>
                                <td><input type='submit' name='aprobarUsuario' class='boton' value='Aprobar' /></td>
                                <td><input type='submit' name='aprobarUsuario' class='boton' value='Bloquear' /></td>
                            </form>
                            </tr>
                            <?php
                        }
                        ?> 
                    </table>
                </fieldset>    
            </div>

            <div id="formEntradas" class="form">
                <fieldset >
                    <legend>Panel de Entradas Nuevas </legend>
                    <table>
                        <?php
                        foreach ($arrayEntradasSinAprobar as $entrada) {
                            ?>
                            <tr>
                            <form id = '<?php $entrada['id'] ?>' method = 'get'>
                                <input type = 'hidden' name = 'idEntrada' value = "<?php echo $entrada['id'] ?>"/>
                                <?php
//                            echo "<a href = 'usuario.php?codigo={$usuario['cod']}'>{$usuario['nombre_corto']}: {$usuario['PVP']} €</a></p>";
                                echo "<td>{$entrada['id']}. {$entrada['texto']}&nbsp &nbsp</td>";
                                ?>
                                <td><input type='submit' name='aprobarEntrada' class='boton' value='Aprobar' /></td>
                                <td><input type='submit' name='aprobarEntrada' class='boton' value='Borrar' /></td>
                            </form>
                            </tr>
                            <?php
                        }
                        ?> 
                    </table>
                </fieldset>
            </div>

            <div id="formComentarios" class="form">
                <fieldset >
                    <legend>Panel de Comentarios Nuevos </legend>
                    <table>
                        <?php
                        foreach ($arrayComentariosSinAprobar as $comentario) {
                            ?>
                            <tr>
                            <form id = '<?php $comentario['id'] ?>' method = 'get'>
                                <input type = 'hidden' name = 'idComentario' value = "<?php echo $comentario['id'] ?>"/>
                                <?php
//                            echo "<a href = 'usuario.php?codigo={$usuario['cod']}'>{$usuario['nombre_corto']}: {$usuario['PVP']} €</a></p>";
                                echo "<td>{$comentario['id']}. {$comentario['texto']}&nbsp &nbsp</td>";
                                ?>
                                <td><input type='submit' name='aprobarComentario' class='boton' value='Aprobar' /></td>
                                <td><input type='submit' name='aprobarComentario' class='boton' value='Borrar' /></td>
                            </form>
                            </tr>
                            <?php
                        }
                        ?> 
                    </table>
                </fieldset>
            </div>
        </section>
    </body>
</html>


<!--<section id="formUsuarios">
    <fieldset >
        <legend>Panel de Usuarios Nuevos </legend>
        <ol>
<?php
//            foreach ($arrayUsuariosSinAprobar as $usuario) {
?>
                <li>
                    <form id = '<?php // $usuario['id']          ?>' method = 'get'>
                        <input type = 'hidden' name = 'idUsuario' value = "<?php // echo $usuario['id']          ?>"/>
<?php
//                            echo "<a href = 'usuario.php?codigo={$usuario['cod']}'>{$usuario['nombre_corto']}: {$usuario['PVP']} €</a></p>";
//                        echo "{$usuario['usuario']}";
?>
                        <input type='submit' name='aprobarUsuario' class='boton' value='Aprobar' />
                        <input type='submit' name='aprobarUsuario' class='boton' value='Bloquear' />
                    </form>
                </li>
<?php
//            }
?> 
        </ol>
    </fieldset>
</section>-->
