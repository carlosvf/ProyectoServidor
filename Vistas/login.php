<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- Desarrollo Web en Entorno Servidor -->
<!-- Tarea 5 : Programación orientada a objetos en PHP -->
<!-- Tienda Web: login.php -->

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>BLOG</title>
    </head>

    <body>
    <link rel="stylesheet" href="../Estilo/estilo.css">
    <div id='login'>
        <form method='post'>
            <fieldset >
                <legend>Login</legend>
                <div><span class='error'><?php echo $error; ?></span></div>
                <div class='campo'>
                    <label for='usuario' >Usuario:</label><br/>
                    <input type='text' name='usuario' id='usuario' maxlength="50" /><br/>
                </div>
                <div class='campo'>
                    <label for='password' >Contraseña:</label><br/>
                    <input type='password' name='password' id='password' maxlength="50" /><br/>
                </div>
                <div id='divBotonesLogin' class='campo' style='text-align: center' >
                    <input type='submit' name='enviarLogin' class='estiloBoton2' value='Loguearse' />
                    <a class='estiloBoton' href="../Controladores/registroControlador.php">Registrarse</a>
                    <a class='estiloBoton' href="../Controladores/blogControlador.php">Acceso al Blog</a>
                </div>
            </fieldset>

        </form>
    </div>

</body>
</html>