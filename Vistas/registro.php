<?php ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- Desarrollo Web en Entorno Servidor -->
<!-- Tarea 5 : Programación orientada a objetos en PHP -->
<!-- Tienda Web: login.php -->

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>REGISTRO</title>
    </head>

    <body>

        <div id='registro'>
            <form method='post'>
                <fieldset >
                    <legend>Registro de usuario</legend>
                    <div class='campo'>
                        <label for='usuario' >Nombre:</label><br/>
                        <input type='text' name='nombre' id='nombre' maxlength="50" /><br/>

                        <label for='correoElectronico' >Email:</label><br/>
                        <input type='email' name='correoElectronico' id='correoElectronico' maxlength="50" /><br/>

                        <label for='nombreUsuario' >Nombre de Usuario:</label><br/>
                        <input type='text' name='nombreUsuario' id='nombreUsuario' maxlength="50" /><br/>

                        <label for='contrasenaRegistro' >Contraseña:</label><br/>
                        <input type='password' name='contrasenaRegistro' id='contrasenaRegistro' maxlength="50" /><br/>

                    </div>
                    <div class='campo' style='text-align: center'>
                        <input type='submit' name='enviarRegistro' class='boton' value='Regristrarse' />
                    </div>
                </fieldset>
            </form>
        </div>

    </body>
</html>