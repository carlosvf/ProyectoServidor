<?php

namespace blog;

use PDO;

require_once '../Clases/Conectar.php';

class Usuario {

    protected $conn;
    protected $atributos = [];

    public function __construct() {
        $this->conn = self::conectar();
    }

    public static function conectar() {
        $db = new Conectar();
        return $db->conexion();
    }

    public function selectYValida($nombreUsuario, $contrasena) {
        try {
            $result = $this->conn->query("SELECT * FROM usuarios WHERE usuario ='" . $nombreUsuario . "' AND contrasena = '" . $contrasena . "';")->fetch(PDO::FETCH_ASSOC);

            if ($result) {
                $this->atributos = $result;
            }
        } catch (PDOException $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }
    
    public function select($id) {
        try {
            $result = $this->conn->query("SELECT * FROM usuarios WHERE id ='" . $id . "';")->fetch(PDO::FETCH_ASSOC);

            if ($result) {
                $this->atributos = $result;
            }
        } catch (PDOException $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }
    

    public function nuevoUsuario($nombre, $correo, $nombreUsuario, $contrasena) {
        $sql = "INSERT INTO usuarios (nombre, email, usuario, contrasena, admin, estado) VALUES (:nombreIntroducido, :emailIntro, :usuarioIntro, :passwordIntro, 'N', 'P')";

        $statement = $this->conn->prepare($sql);
        $statement->bindValue(':nombreIntroducido', $nombre);
        $statement->bindValue(':emailIntro', $correo);
        $statement->bindValue(':usuarioIntro', $nombreUsuario);
        $statement->bindValue(':passwordIntro', $contrasena);
        $statement->execute();
    }

    public function __get($name) {
        if (array_key_exists($name, $this->atributos)) {
            return ($this->atributos[$name]);
        }
    }

    public function __set($name, $value) {
        $this->atributos[$name] = $value;
    }

    public function getAll() {
        return $this->conn->query("SELECT * FROM usuarios ORDER BY id")
                        ->fetchAll();
    }

    public static function gotAll() {

        return self::conectar()->query("SELECT * FROM usuarios ORDER BY id")
                        ->fetchAll();
    }

    public function getbyId($id) {
        return $this->conn->query("SELECT * FROM usuarios WHERE id = '$id'")->fetchAll();
    }

    public function deletebyId($id) {
        try {
            return ($this->conn->exec("DELETE FROM usuarios WHERE id = '$id'"));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public static function deleteAll() {
        try {
            return ($this->conn->exec("TRUNCATE TABLE usuarios"));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public static function getBy($columna, $valor) {
        /// devuelve las tuplas cuya columna sea igual al valor
        return self::conectar()->query("SELECT * FROM usuarios WHERE $columna = '$valor'")->fetchAll();
    }

    public static function deleteBy($columna, $valor) {
        /// borra las tuplas cuya columna sea igual al valor, devuelve tuplas afectadas
        return self::conectar()->query("DELETE FROM usuarios WHERE $columna = '$valor'")->fetchAll();
    }

    public function create() {
        /// inserta tupla en la base de datos
        try {
            $sql = "INSERT into usuarios (";
            // Inserto campos en la sql
            foreach ($this->atributos as $key => $value) {
                $sql .= "$key, ";
            }
            $sql = trim($sql, ',') . ") VALUES (";

            // Inserto values en la sql
            foreach ($this->atributos as $key => $value) {
                $sql .= "'$value', ";
            }
            $sql = trim($sql, ',') . ")";

            // echo $sql;
            return (self::conectar()->exec($sql));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public function save() {
        try {
            $sql = "UPDATE usuarios SET ";
            foreach ($this->atributos as $key => $value) {
                if ($key != 'id') {
                    $sql .= "$key = '$value',";
                } else
                    $id = $value;
            }
            $sql = trim($sql, ',') . " WHERE id = '$id'";
            return ($this->conn->exec($sql));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

}
