<?php

namespace blog;

use PDO;

class Comentario {

    protected $conn;
    protected $atributos = [];
    protected $tabla = 'comentarios';

    public function __construct() {
        // $db = new Conectar(array("driver" => "mysql", "host" => "localhost",
        //     "user" => "root", "pass" => "1234", "database" => "alumnos",
        //     "charset" => "utf8"));
        // $this->conn = $db->conexion();
        $this->conn = self::conectar();
    }

    public static function conectar() {
        $db = new Conectar();
        return $db->conexion();
    }

    public function select($id) {
        try {
            $result = $this->conn->query("SELECT * FROM {$this->tabla} WHERE id =
				'$id'")->fetch(PDO::FETCH_ASSOC);
            $this->atributos = $result;
        } catch (PDOException $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public function __get($name) {
        if (array_key_exists($name, $this->atributos)) {
            return ($this->atributos[$name]);
        }
    }

    public function __set($name, $value) {
        $this->atributos[$name] = $value;
    }

    public function getAll() {
        return $this->conn->query("SELECT * FROM {$this->tabla} ORDER BY id")
                        ->fetchAll();
    }

    public static function gotAll() {
        // $db = new Conectar(array("driver" => "mysql", "host" => "localhost",
        //     "user" => "root", "pass" => "secret", "database" => "alumnos",
        //     "charset" => "utf8"));
        // return $db->conexion()->query("SELECT * FROM alumnos ORDER BY nom")
        //                 ->fetchAll();
        return self::conectar()->query("SELECT * FROM {$this->tabla} ORDER BY id")
                        ->fetchAll();
    }

    public function getbyId($id) {
        return $this->conn->query("SELECT * FROM {$this->tabla} WHERE id = '$id'")->fetchAll();
    }

    public function deletebyId($id) {
        try {
            return ($this->conn->exec("DELETE FROM {$this->tabla} WHERE id = '$id'"));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public static function deleteAll() {
        // borra todas las tuplas
        try {
            return ($this->conn->exec("TRUNCATE TABLE {$this->tabla}"));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public static function getBy($columna, $valor) {
        /// devuelve las tuplas cuya columna sea igual al valor
        return self::conectar()->query("SELECT * FROM comentarios WHERE $columna = '$valor'")->fetchAll();
    }

    public static function deleteBy($columna, $valor) {
        /// borra las tuplas cuya columna sea igual al valor, devuelve tuplas afectadas
        return self::conectar()->query("DELETE FROM {$this->tabla} WHERE $columna = '$valor'")->fetchAll();
    }

    public function create() {
        /// inserta tupla en la base de datos
        try {
            $sql = "INSERT into {$this->tabla} (";
            // Inserto campos en la sql
            foreach ($this->atributos as $key => $value) {
                $sql .= "$key,";
            }
            $sql = trim($sql, ',') . ") VALUES (";

            // Inserto values en la sql
            foreach ($this->atributos as $key => $value) {
                $sql .= "'$value',";
            }
            $sql = trim($sql, ',') . ")";

            // echo $sql;
            return (self::conectar()->exec($sql));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

    public function save() {
        try {
            $sql = "UPDATE {$this->tabla} SET ";
            foreach ($this->atributos as $key => $value) {
                if ($key != 'id') {
                    $sql .= "$key = '$value',";
                } else
                    $id = $value;
            }
            $sql = trim($sql, ',') . "WHERE id = '$id'";
            return ($this->conn->exec($sql));
        } catch (Exception $ex) {
            echo 'Falló la consulta: ' . $ex->getMessage();
        }
    }

}
