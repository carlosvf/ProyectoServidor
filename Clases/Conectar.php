<?php

namespace blog;
use PDO;

class Conectar
{

    private $driver;
    private $host, $user, $pass, $database, $charset;

    public function __construct()
    {
         $this->driver = 'mysql';
        $this->host = 'localhost';
        $this->user = 'root';
        $this->pass = '';
        $this->database = 'blog';
        $this->charset = 'utf8';
    }

    public function conexion()
    {
        try {
            $conn = new PDO("$this->driver:host=$this->host;dbname=$this->database", $this->user, $this->pass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->query("SET NAMES '$this->charset'");
            return $conn;
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
    }

}
?>